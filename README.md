# Url Shortener

## 1. Blockchain

#### 1.1 Install

##### 1.1.1 Install the VeChain Blockchain
Go to <url> to download and build the VeChain blockchain which will run a local node on your computer.

##### 1.1.2 Install Web3-Gear
Go to <url> to download and build the Web3-Gear software which is used to translate web3 calls to VeChain REST calls. This is only required if we want to use Truffle, not if we use VeChain's Sync Browser in combination with the Inspector webapp (`inspector.vecha.in`)

##### 1.1.3 Install Truffle

Go to <url> to install Truffle. Truffle is used to build, test and migrate the smart contracts.

#### 1.2 Start

##### 1.2.1 Start up a local blockchain

Run `/opt/thor/bin/thor solo` to start up a local VeChain blockchain. Make sure to let it sync properly.

#### 1.2.2 Fire up Web3 Gear

Run `web3-gear` to translate truffle's web3 commands to VeChain REST calls.

#### 1.3 Deploy

##### 1.3.1 Test smart contracts

Run `truffle test` to execute the tests on the smart contracts

##### 1.3.2 Compile smart contracts

Run `truffle compile` to compile the smart contracts

##### 1.3.3 Migrate smart contracts

Run `truffle migrate --network thor` to migrate the smart contracts (use --clean to make sure old smart contract data is gone)

## 2. Server

#### 2.1 Install

##### 2.1.1 Install NodeJS

https://nodejs.org/en/download/

##### 2.1.2 Install Typescript

`npm install -g typescript`

##### 2.1.3 Install Yarn

https://yarnpkg.com/lang/en/docs/install

##### 2.1.4 Install dependencies

Go into the `server` folder and run `yarn install`

#### 2.2 Build

Make sure you are in the `server` folder and run `yarn build`

#### 2.3 Start

To start the server make sure you are in `server` folder and run `yarn start`

## 3. Frontend

#### 3.1 Install

##### 3.1.1 Install Node, Typescript and Yarn

Assumes these are installed from H2

##### 3.1.2 Install dependencies

Go into the `frontend` folder and run `yarn install`

#### 3.2 Run

Make sure you are in the `frontend` folder and run `yarn start` 

In order to add URL's, the server should be running (from another terminal)

In order to decentralize URL's, you should go to the frontend from the `Sync` browser that is supplied by VeChain, if you don't the application will prompt you to download it.

To view the contract's events go to https://inspector.vecha.in/#/contracts and add the contract address (`0x46db3d3740Ac318620512dF10b359F3c9286CA3C` for testnet) together with the ABI that can be found in the contract's json file.

