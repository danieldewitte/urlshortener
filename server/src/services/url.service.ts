import {dbUrls} from '../utils/leveldb';
import {getCounter, getCounterTotal} from './counter.service';
import Log from '../utils/log';
import {Url} from '../model/url';
import ShortenUrl from "../utils/shorten.url";

export const getUrl = async (key: string): Promise<string | undefined> => {
    try {
        const url = await dbUrls.find((v) => { return v.key === key});
        if (url) {
            return url.url;
        } else {
            return undefined;
        }
    } catch (e) {
        if (e.message.indexOf('NotFoundError') >= 0)
            Log.error('url.service:getUrl', e);
        return undefined;
    }
};

export const getUrlAtIdx = async (key: string): Promise<Url | undefined> => {
    try {
        return await dbUrls.get(key);
    } catch (e) {
        if (e.message.indexOf('NotFoundError') >= 0)
            Log.error('url.service:getUrl', e);
        return undefined;
    }
};

export const putUrl = async (url: string): Promise<string> => {
    try {
        const counter = await getCounter();
        const key = ShortenUrl.shortenUrl(counter).toString();
        Log.debug('url.service:putUrl', key, url);
        await dbUrls.put(counter, {key: key, url: url});
        return key;
    } catch (e) {
        Log.error('url.service:putUrl', e.message);
        return '-1';
    }
};

export const getRecentUrls = async (): Promise<Url[]> => {

    const urls: Url[] = [];
    const max = await getCounterTotal();
    const amount = max >= 5 ? 5 : max;

    for (let i = 1; i <= amount; i++) {
        const short = max - i;
        const url = await getUrlAtIdx(short.toString());
        if (url) {
            urls.push({key: url.key, url: url.url});
        }
    }

    return urls;
};