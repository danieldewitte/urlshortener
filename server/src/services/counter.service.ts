import {dbCounter} from '../utils/leveldb';
import Log from '../utils/log';

export const getCounter = async (): Promise<string> => {
    try {
        const counter = await dbCounter.get('counter');
        await dbCounter.put('counter', counter+1);
        return counter.toString();
    } catch (e) {
        if (e.message.indexOf('NotFoundError') >= 0) {
            Log.error('url.service:getUrl', e);
            return '-1';
        } else {
            await dbCounter.put('counter', 1);
            return '1';
        }
    }
};

export const getCounterTotal = async (): Promise<number> => {
    try {
        return await dbCounter.get('counter');
    } catch(e) {
        Log.error('url.service:getCounterTotal', e);
        return 0;
    }
};