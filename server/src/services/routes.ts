import {Request, Response} from 'express';
import {getRecentUrls, getUrl, putUrl} from './url.service';
import Log from "../utils/log";

export default [
    {
        path: '/get/:shortUrl',
        method: 'get',
        handler: [
            async (req: Request, res: Response) => {
                const result = await getUrl(req.params.shortUrl);
                res.status(200).send(result);
            }
        ]
    },
    {
        path: '/put',
        method: 'post',
        handler: [
            async ({body}: Request, res: Response) => {
                Log.debug('routes:put', body);
                const result = await putUrl(body.url);
                res.status(200).send(result);
            }
        ]
    },
    {
        path: '/recent',
        method: 'get',
        handler: [
            async (req: Request, res: Response) => {
                const result = await getRecentUrls();
                res.status(200).send(result);
            }
        ]
    },
];
