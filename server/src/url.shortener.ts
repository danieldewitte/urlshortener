import http from 'http';
import express from 'express';
import { applyMiddleware, applyRoutes } from './utils';
import middleware from './middleware';
import errorHandlers from './middleware/errorHandlers';
import routes from './services';
import Log from './utils/log';

process.on('uncaughtException', e => {
    Log.error('url.shortener:on.uncaughtException', e);
    process.exit(1);
});

process.on('unhandledRejection', e => {
    Log.error('url.shortener:on.unhandledRejection', e);
    process.exit(1);
});

const router = express();
applyMiddleware(middleware, router);
applyRoutes(routes, router);
applyMiddleware(errorHandlers, router);

const { PORT = 3035 } = process.env;
const server = http.createServer(router);

server.listen(PORT, () =>
    Log.info(`Server is running on http://localhost:${PORT}...`)
);
