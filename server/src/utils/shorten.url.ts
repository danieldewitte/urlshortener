export default class ShortenUrl {

    public static shortenUrl(url: string, seed: number = 0): string {

        let random = 0xdeafcab5 ^ seed;
        let variance = 0xdefaced ^ seed;

        for (let i = 0; i < url.length; i++) {
            const ch = url.charCodeAt(i);
            random = Math.imul(random ^ ch, 2744554671);
            variance = Math.imul(variance ^ ch, 1696433767);
        }

        random = Math.imul(random ^ random>>>16, 2428625270) ^ Math.imul(variance ^ variance>>>13, 3626849099);
        variance = Math.imul(variance ^ variance>>>16, 2428625270) ^ Math.imul(random ^ random>>>13, 3626849099);

        let key = 4294967296 * (2097151 & variance) + (random>>>0);
        return key.toString(16);
    }
}
