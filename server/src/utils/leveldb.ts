import Level from 'level-ts';
import dotenv from 'dotenv';
import {Url} from "../model/url";

dotenv.config();

const dbPathUrls = process.env.DB_PATH_URLS || 'db-urls';
export const dbUrls = new Level<Url>(dbPathUrls);

const dbPathCounter = process.env.DB_PATH_COUNTER || 'db-counter';
export const dbCounter = new Level<number>(dbPathCounter);
