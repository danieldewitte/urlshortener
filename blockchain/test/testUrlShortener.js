var UrlShortener = artifacts.require("UrlShortener");

contract('UrlShortener', (accounts) => {
  
  const urlShortTest1 = 1
  const urlLongTest1 = 'https://blocklab.io';

  const urlShortTest4 = 2
  const urlLongTest4 = 'https://everscore.io';

  const urlShortTest5 = 3
  const urlLongTest5 = 'https://vechain.org';  

  const urlShortNull = null;
  const urlLongNull = null;

  const Test5Account = accounts[0];

  it ('should successfully add a shortened url', async function () {
    const urlShortener = await UrlShortener.deployed();
    await urlShortener.addShortenedUrl.sendTransaction(urlShortTest1, urlLongTest1);
    const url = await urlShortener.getUrl(urlShortTest1);
    assert.equal(url.toString(), urlLongTest1);
  });

  it ('should not add a shortened url if the short url is empty', async function () {
    const urlShortener = await UrlShortener.deployed();
    let got_error = false;
    try {
      await urlShortener.addShortenedUrl.sendTransaction(urlShortNull, urlLongTest2);
    } catch (e) {
      got_error = true;
    }
    assert.isTrue(got_error);
  });

  it ('should not add a shortened url if the long url is empty', async function () {
    const urlShortener = await UrlShortener.deployed();
    let got_error = false;
    try {
      await urlShortener.addShortenedUrl.sendTransaction(urlShortTest2, urlLongNull);
    } catch (e) {
      got_error = true;
    }
    assert.isTrue(got_error);
  });

  it ('should not be able to add the same shortened url twice', async function () {
    const urlShortener = await UrlShortener.deployed();
    let got_error = false;
    await urlShortener.addShortenedUrl.sendTransaction(urlShortTest4, urlLongTest4);
    try {
      await urlShortener.addShortenedUrl.sendTransaction(urlShortTest4, urlLongTest4);
    } catch (e) {
      got_error = true;
    }
    assert.isTrue(got_error);
  });

  it ('should be able to get the creators address of the shortened url', async function () {
    const urlShortener = await UrlShortener.deployed();
    await urlShortener.addShortenedUrl.sendTransaction(urlShortTest5, urlLongTest5, {
      from: Test5Account
    });

    const creator = await urlShortener.getUrlCreator(urlShortTest5);

    assert.equal(Test5Account, creator);
  });

});
