pragma solidity ^0.5.8;

contract UrlShortener {

    // Fire an event with the urlId whenever a new shortened url is added
    event NewUrl(uint indexed urlId);

    // Mapping from urlId to url
    mapping (uint256 => string) private _urls;

    // Mapping from urlId to url creator address
    mapping (uint256 => address) private _creators;

    // Get the original url by providing the shortened url
    function getUrl(uint256 urlId) public view returns(string memory) {
        return _urls[urlId];
    }

    // Get the address of the wallet that added the shortened url
    function getUrlCreator(uint256 urlId) public view returns(address) {
        return _creators[urlId];
    }

    // Add a new shortened url
    function addShortenedUrl(uint256 urlId, string memory url) public {
        require(bytes(_urls[urlId]).length == 0, 'urlId is already taken');
        _urls[urlId] = url;
        _creators[urlId] = msg.sender;
        emit NewUrl(urlId);
    }

}