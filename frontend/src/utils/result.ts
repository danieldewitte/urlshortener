import {Log} from './log';

export class ErrorResult {
  constructor(public error: string) {}
  public print (): ErrorResult {
    Log.debug('ErrorResult', this.error);
    return this;
  }
}

export type Result<T> = T | ErrorResult;
