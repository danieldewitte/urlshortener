export function sleep(time: number): Promise<void> {
    return new Promise((accept) => {
        setTimeout(accept, time);
    });
}
