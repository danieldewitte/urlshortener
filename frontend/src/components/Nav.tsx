import React, {Component} from "react";
import logo from "../everscore_logo.png";
import NewUrlForm from "./NewUrlForm";
import VerifyUrlForm from "./VerifyUrlForm";
import './Nav.css'
import {NavMenu} from "./NavMenu";
import Divider from '@material-ui/core/Divider';

type State = {
    displayMode: number
}

export default class Nav extends Component<{}, State> {

    constructor(props: State) {
        super(props);

        this.state = {
            displayMode: 0
        };
    }

    private onClick = (item: string) => {
        if (item === 'verify') {
            this.setState({
                displayMode: 1
            })
        } else if (item === 'add') {
            this.setState({
                displayMode: 0
            })
        }
    };

    render() {
        return (
            <div>
                <header className='header'>
                    <div className="introduction">
                        EverURL
                    </div>

                    <img src={logo} className='logo' alt='logo'/>
                </header>
                <Divider/>
                <NavMenu clicked={this.onClick}/>
                <Divider/>
                <p className="subtitle"><i>
                    A decentralized Url Shortener on the VeChainThor blockchain.
                </i></p>

                {this.state.displayMode === 0 &&
                <NewUrlForm/>
                }

                {this.state.displayMode === 1 &&
                <VerifyUrlForm/>
                }
            </div>
        )
    }
}
