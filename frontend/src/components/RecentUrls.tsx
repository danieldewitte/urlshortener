import React, {Component} from "react";
import {Url} from "../models/url";
import {UrlList} from "./UrlList";

type State = {
    returnedKey: string;
    urls: Url[];
}

export default class RecentUrls extends Component<{}, State> {

    private serverUrl = process.env.REACT_APP_ADD_URL;

    constructor(props: State) {
        super(props);

        this.state = {
            returnedKey: '',
            urls: []
        }
    }

    componentDidMount() {
        this.getRecentUrls();
    }

    public getRecentUrls() {
        fetch(this.serverUrl + '/recent')
            .then(res => res.json() )
            .then((data) => {
                console.log('FetchRecent', data);
                this.setState({
                    urls: data
                });

            })
            .catch(error => console.log('FetchRecent', error));
    }

    private urlClicked = async (url: Url) => {
        console.log('clicked', url.url);
    };

    render() {
        return (
            <div className="center">
                <div className="urlList">
                    {!this.state.urls && <div>Loading...</div>}
                    {this.state.urls &&
                    <div>
                        <h3>Recent Url's</h3>
                        <UrlList urls={this.state.urls} clicked={this.urlClicked}/>
                    </div>
                    }
                </div>
            </div>
        )
    }
}