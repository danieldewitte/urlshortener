import React, {FunctionComponent} from 'react';
import {UrlListItem} from './UrlListItem';
import List from '@material-ui/core/List';
import {Url} from '../models/url';

type UrlListProps = {
    urls: Url[];
    clicked: (url: Url) => void;
}

export const UrlList: FunctionComponent<UrlListProps> = ({urls, clicked}) => (
    <List component="nav">
        {urls.map(url => (
            <UrlListItem key={url.key.toString()} url={url} clicked={clicked}/>
        ))}
    </List>
);
