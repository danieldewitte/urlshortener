import React, {Component} from 'react';
import {Button, TextField} from "@material-ui/core";
import './NewUrlForm.css'
import {UrlShortenerContractService} from "../services/url.shortener.contract.service";
import {ErrorResult} from "../utils/result";

type State = {
    inputHash: string;
    displayText: string;
    returnedUrlServer: string;
    serverText: string;
    returnedUrlSmartContract: string;
    smartContractText: string;
    matches: boolean;
}

export default class VerifyUrlForm extends Component<{}, State> {

    private serverUrl = process.env.REACT_APP_ADD_URL;

    private textColor = {
        color: "black"
    };

    constructor(props: State) {
        super(props);

        this.state = {
            inputHash: '',
            displayText: '',
            returnedUrlServer: '',
            serverText: '',
            returnedUrlSmartContract: '',
            smartContractText: '',
            matches: false
        };
    }

    private async handleButtonClick(event: React.MouseEvent<HTMLButtonElement>) {

        if (this.state.inputHash !== '') {

            this.setState({
                displayText: 'Fetching from server ...'
            });

            fetch(this.serverUrl + '/get/' + this.state.inputHash)
            .then(res => res.text())
            .then((data) => {
                this.setState({
                    returnedUrlServer: data,
                    serverText: data ? 'Url on the server: ' + data : 'Url not stored on the server',
                    displayText: 'Fetching from blockchain ...'
                });
                UrlShortenerContractService.getInstance().getUrl(this.state.inputHash)
                .then((result) => {
                    if (!(result instanceof ErrorResult)) {
                        this.setState({
                            returnedUrlSmartContract: result.url,
                            smartContractText: result.url ? 'Url on the blockchain: ' + result.url : 'Url not stored on the blockchain',
                            matches: !!(data === result.url && data && result.url),
                            displayText: !!(data === result.url && data && result.url) ? 'Both Url\'s are the same!' : (data && result.url ? 'Blockchain and Server display inconsistencies!' : '')

                        });
                        this.textColor = {
                            color: this.state.matches ? "green" : "red"
                        };
                    }
                })
                .then(() => {
                    console.log(this.state.matches);
                    this.setState({

                    });
                });
            });
        }
    }

    private handleUrlChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        this.setState({
            inputHash: event.target.value,
            serverText: '',
            smartContractText: ''
        });
    }

    render() {

        const textColor = {
            color: this.state.matches ? "green" : "red"
        };

        console.log(textColor, this.state.matches);

        return (
            <div>
                <form className="urlForm" noValidate autoComplete="off">
                    <div>
                        <TextField
                            id="url-textfield"
                            className="urlTextField"
                            label="Enter Shortened URL Hash"
                            margin="normal"
                            onChange={(e) => this.handleUrlChange(e)}
                            variant="outlined"
                        />
                    </div>

                    <div>
                        <Button
                            className="addUrlButton"
                            variant="contained"
                            color="primary"
                            disabled={this.state.inputHash === ''}
                            onClick={(e) => this.handleButtonClick(e)}>
                            Verify Url
                        </Button>
                    </div>
                </form>

                <div className="result">
                    {this.state.displayText && <p>{this.state.displayText}</p>}
                    {this.state.serverText && <p style={textColor}>{this.state.serverText}</p>}
                    {this.state.smartContractText && <p style={textColor}>{this.state.smartContractText}</p>}
                </div>
            </div>
        )
    }
}
