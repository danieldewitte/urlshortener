import React, {FunctionComponent} from 'react';
import { Url } from '../models/url';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import LinkIcon from '@material-ui/icons/Link';

type UrlListItemProps = {
    url: Url;
    clicked: (url: Url) => void;
}

export const UrlListItem: FunctionComponent<UrlListItemProps> = ({url, clicked}) => {

    const onClick = () => {
        clicked(url);
    };

    return (
        <a href={url.url}>
            <ListItem button onClick={onClick}>
                <ListItemIcon>
                    <LinkIcon />
                </ListItemIcon>
                <ListItemText primary={url.key.toString()} secondary={url.url ? url.url.substr(0,48) : ''} />
            </ListItem>
        </a>
    );
};
