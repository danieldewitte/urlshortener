import React, {Component} from 'react';
import {Button, Checkbox, FormControlLabel, TextField} from "@material-ui/core";
import './NewUrlForm.css'
import {UrlShortenerContractService} from "../services/url.shortener.contract.service";
import RecentUrls from "./RecentUrls";

type State = {
    decentralize: boolean;
    inputUrl: string;
    returnedKey: string;
    invalidUrl: boolean;
}

export default class NewUrlForm extends Component<{}, State> {

    private serverUrl = process.env.REACT_APP_ADD_URL;
    private urlRegEx = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/;
    private recentUrls: React.RefObject<RecentUrls>;

    constructor(props: State) {
        super(props);

        this.state = {
            decentralize: false,
            inputUrl: '',
            returnedKey: '',
            invalidUrl: true
        };

        this.recentUrls = React.createRef()
    }

    private validateURL(text:string) {
        return this.urlRegEx.test(text);
    }

    private handleCheckboxClick(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({decentralize: event.target.checked});
    }

    private handleButtonClick(event: React.MouseEvent<HTMLButtonElement>) {
        console.log(this.state.decentralize, this.state.inputUrl);

        if (this.state.inputUrl !== '' && !this.state.invalidUrl) {

            fetch(this.serverUrl + '/put', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    url: this.state.inputUrl
                })
            })
                .then(res => res.text())
                .then((data) => {
                    this.setState({
                        returnedKey: data
                    });
                    this.recentUrls.current!.getRecentUrls();
                    if(this.state.decentralize) {
                        UrlShortenerContractService.getInstance().addUrl(this.state.returnedKey, this.state.inputUrl);
                    }
                });
        }
    }

    private handleUrlChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        this.setState({inputUrl: event.target.value});
        if(this.validateURL(event.target.value)) {
            this.setState({invalidUrl: false});
        } else {
            this.setState({invalidUrl: true});
        }
    }

    render() {
        return (
            <div>

                <form className="urlForm" noValidate autoComplete="off">
                    <div>
                        <TextField
                            error={this.state.invalidUrl && this.state.inputUrl !== ''}
                            id="url-textfield"
                            className="urlTextField"
                            label="Enter URL"
                            margin="normal"
                            onChange={(e) => this.handleUrlChange(e)}
                            variant="outlined"
                            helperText={this.state.invalidUrl && this.state.inputUrl !== '' ? 'Invalid URL.' : ''}
                        />
                    </div>

                    <div>
                        <Button
                            className="addUrlButton"
                            variant="contained"
                            color="primary"
                            disabled={this.state.invalidUrl}
                            onClick={(e) => this.handleButtonClick(e)}>
                            Add Url
                        </Button>

                        <FormControlLabel
                            className="checkbox"
                            control={
                                <Checkbox
                                    checked={this.state.decentralize}
                                    onChange={(e) => this.handleCheckboxClick(e)}
                                    value="checkedDecentralized"
                                    color="primary"
                                    inputProps={{
                                        'aria-label': 'checkbox',
                                    }}
                                />
                          }
                          label="Decentralize"
                        />
                    </div>

                    {this.state.returnedKey &&
                        <div>
                            <p>Your shortened url is: {window.location.protocol}//{window.location.host}/{this.state.returnedKey}</p>
                        </div>
                    }
                </form>

                <RecentUrls ref={this.recentUrls}/>

            </div>
        )
    }
}
