import React, {FunctionComponent} from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

type NavProps = {
    clicked: (item: string) => void;
}

export const NavMenu: FunctionComponent<NavProps> = ({clicked}) => {

    const onClick = (item: string) => {
        clicked(item);
    };

    const [value, setValue] = React.useState(0);

    return (
        <BottomNavigation
            value={value}
            onChange={(event, newValue) => {
                setValue(newValue);
            }}
            showLabels
        >
            <BottomNavigationAction
                label="Shorten Url"
                icon={<AddCircleIcon />}
                onClick={event => onClick('add')}
            />
            <BottomNavigationAction
                label="Verify Url"
                icon={<CheckCircleIcon />}
                onClick={event => onClick('verify')}
            />
        </BottomNavigation>
    );
};
