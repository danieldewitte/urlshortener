import {Log} from '../utils/log';
import {ContractMethods} from './contractMethods';
import {Url} from '../models/url';
import {ConnexService} from './connex.service';
import {ErrorResult, Result} from '../utils/result';

export class UrlShortenerContractService {

    private static instance: UrlShortenerContractService;

    private constructor() {
    }

    public static getInstance() {
        if (!UrlShortenerContractService.instance) {
            UrlShortenerContractService.instance = new UrlShortenerContractService();
        }
        return UrlShortenerContractService.instance;
    }

    private cs = ConnexService.getInstance();

    async getUrl(key: string): Promise<Result<Url>> {
        Log.debug('url.shortener.contract.service:getUrl', 'invoke with key ' + key);
        const url = await this.cs.call(ContractMethods.GetUrl, key);
        if (url instanceof ErrorResult) {
            Log.error('url.shortener.contract.service:getUrl', url.error);
            return url;
        }
        return {
            key: key,
            url: this.cs.extractDecodedValue(url, '0')
        };
    }

    async getUrlCreator(key: string): Promise<Result<string>> {
        Log.debug('url.shortener.contract.service:getUrlCreator', 'invoke with key ' + key);
        const creator = await this.cs.call(ContractMethods.GetUrlCreator, key);
        if (creator instanceof ErrorResult) {
            Log.error('url.shortener.contract.service:getUrlCreator', creator.error);
            return creator;
        }
        return this.cs.extractDecodedValue(creator, '0');
    }

    async addUrl(key: string, url: string): Promise<Result<true>> {
        Log.debug('url.shortener.contract.service:addUrl', 'invoke with key', key, 'url:', url);
        const clause = await this.cs.makeClause(ContractMethods.AddUrl, key, url);
        if (clause instanceof ErrorResult) {
            Log.error('url.shortener.contract.service:addUrl', clause.error);
            return clause;
        }
        return await this.cs.transaction([clause]);
    }

}
