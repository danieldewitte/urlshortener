import urlShortenerContractArtifact from '../contracts/UrlShortener.json';

export interface ContractMethod {
    name: string;
    gas: number;
    address: string;
    contractAbi: ABIDef;
    method?: Connex.Thor.Method;
}

export interface ABIDef {
    [index: string]: any;
}

export class ContractMethods {

    private static urlContractAddress = process.env.REACT_APP_URL_CONTRACT_ADDRESS || '';

    // set
    static AddUrl: ContractMethod = { name: 'addShortenedUrl', gas: 0, address: ContractMethods.urlContractAddress, contractAbi: urlShortenerContractArtifact.abi };
    static AddCompany: ContractMethod = { name: 'addCompany', gas: 0, address: ContractMethods.urlContractAddress, contractAbi: urlShortenerContractArtifact.abi };

    // get
    static GetUrl: ContractMethod = { name: 'getUrl', gas: 0, address: ContractMethods.urlContractAddress, contractAbi: urlShortenerContractArtifact.abi };
    static GetUrlCreator: ContractMethod = { name: 'getUrlCreator', gas: 0, address: ContractMethods.urlContractAddress, contractAbi: urlShortenerContractArtifact.abi };
}
