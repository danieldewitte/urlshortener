import {Log} from '../utils/log';
import {ABIDef, ContractMethod} from './contractMethods';
import {ErrorResult, Result} from '../utils/result';

export interface EverClause extends Connex.Thor.Clause {
    contractMethod: ContractMethod;
}

interface DecodedReturn {
    [index: string]: any;
}

export class ConnexService {

    private static instance: ConnexService;

    private constructor() {
    }

    public static getInstance() {

        if (!('connex' in window)) {
            window.location.href = 'https://env.vechain.org/r/#' + encodeURIComponent(window.location.href);
        }

        if (!ConnexService.instance) {
            ConnexService.instance = new ConnexService();
        }

        return ConnexService.instance;
    }

    private _signingService = connex.vendor.sign('tx');
    private _ticker = connex.thor.ticker();

    public async call(method: ContractMethod, ...args: any[]): Promise<Result<Connex.Thor.VMOutput>> {

        const cm = await this.contractMethod(method);
        if (cm) {
            const result = await cm.call(...args);
            if (result.reverted) {
                return new ErrorResult(ConnexService.getRevertedReason(result));
            }
            return result;
        }
        return new ErrorResult('Invalid contractMethod');
    }

    public async makeClause(method: ContractMethod, ...args: any[]): Promise<Result<EverClause>> {

        const cm = this.contractMethod(method);
        if(cm) {
            const simulated = await cm.call(...args);
            if (simulated.reverted) {
                const reason = ConnexService.getRevertedReason(simulated);
                Log.error('connex.service.makeClause', 'transaction simulation was reverted', reason);
                return new ErrorResult(reason);
            }

            const clause = cm.asClause(...args) as EverClause;
            clause.contractMethod = method;
            return clause;
        }

        return new ErrorResult('Invalid contractMethod');
    }

    public async transaction(clauses: EverClause[]): Promise<Result<true>> {

        return await (async () => {
            try {
                const txid = await this.sign(clauses);
                if (txid) {
                    const receipt = await this.getTransactionReceipt(txid);
                    Log.debug('connex.service:transaction', 'receipt', receipt);
                    if (receipt.reverted) {
                        return new ErrorResult('The transaction was reverted.');
                    }
                    return true;
                }
                return new ErrorResult('The transaction was not signed.');
            } catch (e) {
                Log.error('connex.service:transaction', 'error:', e);
                return new ErrorResult(e);
            }
        })();
    }

    public extractDecodedValue(output: { decoded?: object | undefined }, key: string) {

        if (output == null) {
            Log.error('connex.service:extractDecodedValue', 'output was null', output);
            return undefined;
        }
        if (output.decoded == null) {
            Log.error('connex.service:extractDecodedValue', 'output.decoded was null', output);
            return undefined;
        }
        return (output.decoded as DecodedReturn)[key];
    }

    private async getTransactionReceipt(txid: string): Promise<Connex.Thor.Receipt> {

        const tx = connex.thor.transaction(txid);

        // TODO: maybe set max number of ticks to wait for the receipt?
        while (true) {
            await this._ticker.next();
            const receipt = await tx.getReceipt();
            if (receipt == null) {
                continue;
            }

            return receipt;
        }
    }

    private async sign(clauses: EverClause[]): Promise<string | null> {

        const useGas = await this.calculateGas(clauses);

        try {
            const safeClauses: Connex.Thor.Clause[] = [];
            for (const c of clauses) {
                safeClauses.push({
                    to: c.to,
                    value: c.value,
                    data: c.data
                });
            }

            const txResponse = await this._signingService.gas(useGas).request(safeClauses);

            if (txResponse != null) {
                return txResponse.txid;
            } else {
                Log.info('connex.service:sign', 'Unable to perform signing request, no transaction response.');
                return null;
            }
        } catch (e) {
            Log.error('connex.service:sign', e);
            return null;
        }
    };

    private async calculateGas(clauses: EverClause[]): Promise<number> {
        const outputs = await connex.thor.explain()
            .gas(2000 * 10000)
            .execute(clauses);
        return Math.round(outputs.reduce((sum, out) => sum + out.gasUsed, 0) * 1.7);
    };

    private contractMethod(clause: ContractMethod): Connex.Thor.Method | null {
        if (connex) {
            try {
                if (!clause.method) {
                    const contract = connex.thor.account(clause.address);
                    const methodAbi = this.findInABI(clause.name, clause.contractAbi);
                    clause.method = contract.method(methodAbi);
                }
                return clause.method;
            } catch (e) {
                Log.error('connex.service:contractMethod', 'Could not find method "' + clause.name + '" in ABI', e);
                return null;
            }
        }
        return null;
    };

    private static getRevertedReason(output: Connex.Thor.VMOutput): string {
        if (output.reverted || output.vmError) {
            if (output.decoded && output.decoded.revertReason) {
                return output.decoded.revertReason;
            }
            return output.vmError;
        }
        return 'reverted';
    }

    private findInABI(name: string, abi: ABIDef) {
        const ABI = abi.find((current: any) => {
            return current.name === name;
        });
        if (!ABI) {
            throw new Error('No ABI found');
        }
        return ABI;
    };
}