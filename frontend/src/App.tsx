import React, {Component} from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Nav from "./components/Nav";

class App extends Component<{}> {
    render() {
        return (
            <BrowserRouter>
                <div className='App'>
                    <Switch>
                        <Route exact path="/:shortUrl"
                               render={ (props) => {
                                   fetch(process.env.REACT_APP_ADD_URL + '/get' + props.location.pathname)
                                       .then(res => res.text())
                                       .then((data) => {
                                           console.log('GetUrl', data);
                                           window.location.href = data;
                                       })
                                       .catch(err => console.log('error while fetching', err));

                                   return <div className='redirecting'>Redirecting</div>;
                               }}
                        />
                        <Route exact path="/"
                               component={Nav}/>
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

export default App;
